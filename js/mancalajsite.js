﻿// JavaScript source code
var pitsnum; var seednum; //משתנים שלתוכם מקליד המשתמש את מספר הגומות והגולות הרצויות
var temppits; // משתנה עזר שנועד להכיל את מספר הגומות הרצויות, וככה לא משתבש אף פעם המספר המקורי
var turn = true; // לדעת איזה תור במשחק
var names = true; var start = true; // נועדו כדי לבדוק אם הייתה כבר את ההגדרה הראשונית של המשחק
var p1name = ""; var p2name = ""; // משתנים שנועדו להכיל את השמות של השחקנים
var AudioWin = new Audio('sounds/mancwin.wav'); // צליל נצחון
var AudioTie = new Audio('sounds/manctie.mp3'); // צליל תיקו
var mancala = []; // מערך מנקלה
var countpits; //נועד כדי לדעת באיזה גומה נמצאים כאשר מכניסים את הכדורים
var anotherturn = [];
var checkk = "";

function SetGameStart() { // פונקציה שבונה את מערך המשחק לפי רצון המשתמש
    checkk = prompt("Please press y or n if you want a special case");
    pitsnum = prompt("How many pits do you want for each row? (3-8)");
    if (checkk != "y") {
        seednum = prompt("How many seeds do you want for each pit? (3-8)");
        
    }
    else {
       seednum = "4";
    }
    if (pitsnum < 3 || pitsnum > 8 || seednum < 3 || seednum > 8 || pitsnum === "" || seednum === "") {
        alert("You must enter a number between 3 to 8");
        SetGameStart();
    }
    else {
        if (checkk === "y") {
            for (var row = 0; row < 2; row++) {
                var temp = [];
                temppits = Number(pitsnum);
                for (var col = 0; col < (temppits + 1); col++) {
                    if ((row === 0 && col === temppits) || (row === 1 && col === 0))
                        temp.push(Number(prompt("How many seeds do you want for [" + row + "," + col + "]?")));
                    else
                        temp.push(Number(prompt("How many seeds do you want for [" + row + "," + col + "]?")));
                    if (row === 1 && col != 0) { //מערך בשביל לבדוק היכן איזו גומה יכולה להביא לתור נוסף
                        anotherturn.push(Number(0));
                    }
                }
                mancala.push(temp);
                turn = false;//////////////

            }
        }
        else {
            for (var row = 0; row < 2; row++) {
                var temp = [];
                temppits = Number(pitsnum);
                for (var col = 0; col < (temppits + 1); col++) {
                    if ((row === 0 && col === temppits) || (row === 1 && col === 0))
                        temp.push(Number(0));
                    else
                        temp.push(Number(seednum));
                    if (row === 1 && col != 0) { //מערך בשביל לבדוק היכן איזו גומה יכולה להביא לתור נוסף
                        anotherturn.push(Number(0));
                    }
                }
                mancala.push(temp);

            }
        }
        start = false;
    }
}

function SetPlayersNames() { // פונקציה שנותנת לשחקנים להקליד את שמם
    p1name = prompt("Player 1, please enter your name:");
    p2name = "Computer";
    if (p1name != "" && p2name != "") {
        names = false;
        if (checkk === "y") {
            document.getElementById("turns").innerHTML = "It's the computer's turn! Click here -> ";
            document.getElementById("CT").style.display = "inline-block";
        }
        else
            document.getElementById("turns").innerHTML = "It's your turn, " + p1name;
    }
    else {
        alert("You must enter your name!");
        SetPlayersNames();
    }

}

function changeBackground(color) { //פונקציה לצבע רקע
    document.body.style.background = color;
}
window.addEventListener("load", function () { changeBackground('lavender'); });


function BuildBoard() { // פונקציה שבונה את לוח המשחק

    if (start === true)
    SetGameStart();

    if (names === true)
        SetPlayersNames();

    var num = 0;
    //var textImgTag = "";
    var textspecial = "<div class='board'> <div class='player1name'>" + p1name + "</div>";
    var text = "";
   
    document.getElementById("Start").innerHTML = "";
   
    for (var row = 0; row < 2; row++) {
        text = text + "<div class='row'>";
        for (var col = 0; col < (temppits + 1); col++) {
            if ((row === 0 && col === temppits) || (row === 1 && col === 0)) {
                if (row === 0 && col === temppits) {
                    textspecial = textspecial + "<div class='containerStore'>" + "<div class='storep1' id='" + num + "' ; onclick='StartGame(this);'> <div class='label'>" + mancala[row][col] + " </div> </div> " + "</div>";

                }
                else if (row === 1 && col === 0) {
                    textspecial = textspecial + "<div class='containerStore'>" + "<div class='storep2' id='" + num + "' ; onclick='StartGame(this);'> <div class='label'>" + mancala[row][col] + "</div>  </div > " + "</div>";
                }
                num++;

                    
            }
            else {
                text = text + "<div class='pit' id='" + num + "' ;' onclick='StartGame(this);'> <div class='label'>" + mancala[row][col] + "</div> </div > ";
                num++;
            }

            

        }
        text = text + "</div>";

    }
    text = text + "<div class='player2name'>" + p2name + "</div>" + "</div>";
    document.getElementById("Start").innerHTML = textspecial + text;


    var whichseed = 0; // משתנה שנועד לזיהוי כל גולה וגולה
    var i = 0; // משתנה שנועד לזיהוי כל גומה וגומה
    // פועל בעצם כמו num למעלה
    for (var row = 0; row < 2; row++) {
        for (var col = 0; col < (temppits + 1); col++) {
            var tempnum = mancala[row][col]; // משתנה לדעת כמה גולות יש בגומה ספציפית
            if (tempnum != 0) {
                AddSeed(tempnum, i, whichseed);
                whichseed += tempnum;
              
            }
            i++;

        }
    }

}



function AddSeed(seed, whichpit, whichseed) { // פונקציה שמוסיפה את מספר הגולות שאמורות להיות בגומה
    for (var i = 0; i < seed; i++) {

        var pitwidth = document.getElementById(whichpit).clientWidth;
        var pitheight = document.getElementById(whichpit).clientHeight;

        document.getElementById(whichpit).innerHTML += "<div class='seed' id='s" + whichseed + "'></div>"; // הוספה של גולה 

        document.getElementById("s" + whichseed).style.top = getRandomNumber(40, pitheight-50) + "px"; // הגדרת המיקום של הגולה באופן רנדומלי
        document.getElementById("s" + whichseed).style.left = getRandomNumber(15, pitwidth - 50) + "px"; // הגדרת המיקום של הגולה באופן רנדומלי

        document.getElementById("s" + whichseed).style.backgroundColor = "rgb(" + getRandomNumber(0, 255) + "," + getRandomNumber(0, 255) + "," + getRandomNumber(0, 255) + ")";
        // הגדרת הצבע של הגולה באופן רנדומלי
        document.getElementById("s" + whichseed).style.zIndex = i; // נועד בשביל שהגולות יוכלו לשבת אחת על השנייה
        
        whichseed++;


    }
}
//מערכים למיקום וצבע

function getRandomNumber(low, high) { // פונקציה שמחזירה ערך רנדומלי בתחום מוגדר של מספרים (כולל)
    var r = Math.floor(Math.random() * (high - low + 1)) + low;
    return r;
}


var anotherboard; //for DidTheGameEndAfterMove function.

var saveamuda; ///  שומר את העמודה שבה המחשב יבצע את המהלך הכי טוב 
var minvalue; //// 
var maxvalue; //foranotherturn
var ezerboard; /// נועד לשמור את הלוח אחרי שבוצע עליו מהלך אחד
var saveboard; /// לוח לבדיקה
var ezerboard2; /// נועד לשמור את הלוח אחרי שבוצעו עליו שני מהלכים
var boardcheck; /// הלוח שעליו יעשו הבדיקות 
var whichcolzero = []; //שומר איזה עמודות היו ריקות לפני שבוצעו מהלכים
//כך יהיה אפשר לתת ניקוד אם המחשב ממלא מקום שהשחקן הראשון היה יכול לאכול ממנו

function WhichColIsZero(ezerboard) { // פונקציה ששומרת את המקומות הריקים בצד של שחקן 1
                                     // שמהם יש סיכוי שהוא יגנוב למחשב גולות 
    var i; 
    for (i = 0; i < temppits; i++) {
        if (ezerboard[0][i] === 0) {
            var d;
            for (d = 0; d < i; d++) {
                if (ezerboard[0][d] === (i - d)) {
                    whichcolzero.push(i);
                    break;
                }
            }
           

        }
    }
   
}
var anotherturnboolp2 = false; // תור נוסף למחשב
var anotherturnboolp1 = false; // תור נוסף לשחקן 1
var arrturn = []; //מערך ששומר בתוכו את הערכים שמתוכם המחשב יבחר את המהלך הכי טוב
var first = true; var second = true;
var copythisarray;
var clearthetables = true;

function ShowTheArray(copyarray) {
    if (clearthetables === true) {
        document.getElementById("HTMLshowarray").innerHTML = "";
        clearthetables = false;
    }
    var text = "<table border='1' align='center' style='display:inline-block'; margin:4vmin;>";
    for (var row = 0; row < 2; row++) {
        text = text + "<tr>";
        for (var col = 0; col < (temppits + 1); col++) {
            text = text + "<td>" + copyarray[row][col] + "</td>";
        }
        text = text + "</tr>";

    }
    text = text + "</table> &nbsp";
    document.getElementById("HTMLshowarray").innerHTML += text;

}

function TurnComputer() {
    if (!turn) {
  
        WhichColIsZero(mancala); // הסבר למעלה
        var k; var p; arrturn.push(Number(temppits * Number(seednum) * -2));
        for (k = 1; k < temppits + 1; k++) { // כל מהלך אפשרי ראשון של המחשב
            minvalue = temppits * Number(seednum) * 2; maxvalue = minvalue * -1;
            value = 0;
            ezerboard = CopyArray(mancala);
            if (ezerboard[1][k] != 0) { //אם בכלל אפשר לבצע מהלך
                CheckPlayer2(k, ezerboard); //ביצוע מהלך
                if (first === true) {
                    copyarray = CopyArray(ezerboard);
                    ShowTheArray(copyarray);
                    first = false;
                }
                if (anotherturnboolp2 === true) { //אם תור נוסף בעקבות המהלך המדומה של המחשב
                    anotherturnboolp2 = false;
                    var t;
                    for (t = 1; t < temppits + 1; t++) { //ביצוע תור נוסף
                        if (ezerboard[1][t] != 0) {
                            ezerboard2 = CopyArray(ezerboard);
                            CheckPlayer2(t, ezerboard2);//ביצוע מהלך
                            if (second === true) {
                                copyarray = CopyArray(ezerboard2);
                                ShowTheArray(copyarray);
                            } 
                            boardcheck = ezerboard2;
                            var value = ValueBoard(boardcheck); //שליחה לפונקציה שמנקדת את הלוח
                            if (value > maxvalue)
                                maxvalue = value;
                        }
                        
                    }
                    second = false;
                    anotherboard = ezerboard;
                    DidTheGameEndAfterThisMove(anotherboard); //בדיקה אם בכלל היה תור נוסף אחרי - יכול להיות שנגמר המשחק 
                    if (endafterturn === true) { // אם נגמר המשחק אחרי מהלך אחד
                        arrturn.push(-1 * maxvalue);
                    } else
                        arrturn.push(maxvalue); //מכניס את המהלך שהכי טוב למחשב בגלל תור נוסף
                }
                else { //אם אין תור נוסף למחשב

                    for (p = 0; p < temppits; p++) { //מהלכים מדומים של השחקן
                        if (ezerboard[0][p] != 0) {
                            ezerboard2 = CopyArray(ezerboard);
                            CheckPlayer1(p, ezerboard2);
                            if (second === true) {
                                copyarray = CopyArray(ezerboard2);
                                ShowTheArray(copyarray);
                            }
                            boardcheck = ezerboard2;
                            var value = ValueBoard(boardcheck); // שליחה לפונקציה שמנקדת את הלוח 
                            if (value < minvalue)
                                minvalue = value;
                        }
                     
                    }
                    second = false;
                    arrturn.push(minvalue); //מכניס את המהלך שהכי טוב לשחקן 1
                }
                
            }
            else
                arrturn.push(Number(temppits * Number(seednum) * -2)); //אם אי אפשר לבצע מהלך למערך חייב להיכנס הערך הכי קטן       
                                                                       // כדי שידולג בבחירת המהלך
        }
        var max = Number(temppits * Number(seednum) * -2); //נועד שיהיה אפשר להשוות 
        var noseeds = Number(temppits * Number(seednum) * -2); //גומה ללא גולות מקבלת ערך של מינוס מספר הגולות הכולל בכל המשחק
        for (var i = 1; i < Number(arrturn.length - 1); i++) { //לולאה שמשווה בין כל הערכים של המהלכים ושומרת את הגומה שבה יש את המהלך הכי טוב
            if (arrturn[i] > arrturn[i + 1] && arrturn[i] > max) { 
                saveamuda = i;
                max = arrturn[i];
            }
            else if (arrturn[i + 1] != noseeds && arrturn[i + 1] > max) {
                saveamuda = i + 1;
                max = arrturn[i + 1];
            }
        }
        ezerboard = CopyArray(mancala);
        CheckPlayer2(saveamuda, ezerboard); //ביצוע המהלך האמיתי
        saveboard = ezerboard;
        if (anotherturnboolp2 === true) { //אם יש תור נוסף
            anotherturnboolp2 = false;
            turn = false;
            alert(p2name + " gets another turn!");
            document.getElementById("turns").innerHTML = "It's the computer's turn! Click here -> ";
            document.getElementById("CT").style.display = "inline-block";
        }
        else  //אם אין תור נוסף
            turn = true;
        mancala = saveboard; //לוח המנקלה מתעדכן ללוח שהתבצע עליו המהלך הכי טוב שנבחר
        var savenumanimation = temppits + 2 + saveamuda-1; //בשביל האנימציה
        document.getElementById(savenumanimation).style.animation = "blink 1s 2";
        setTimeout("BuildBoard()", 2000);
        arrturn.length = 0;
        whichcolzero.length = 0;
        first = true;
        second = true;
        clearthetables = true;
    }
    IsEndGame();
}

var endafterturn; 
function DidTheGameEndAfterThisMove(anotherboard) { //בדיקה אם המשחק הסתיים אחרי מהלך אחד 
    endafterturn = true;
    var t;
    for (t = 1; t < temppits + 1; t++) {
        if (anotherboard[1][t] != 0) {
            endafterturn = false;
        }
    }
}


function ChangeSeed() {
    var idd = 0;
    var whichseed = 0; // משתנה שנועד לזיהוי כל גולה וגולה
    var i = 0; // משתנה שנועד לזיהוי כל גומה וגומה
    // פועל בעצם כמו num למעלה
    for (var row = 0; row < 2; row++) {
        for (var col = 0; col < (temppits + 1); col++) {
            document.getElementById(i).innerHTML = "";
            var tempnum = mancala[row][col]; // משתנה לדעת כמה גולות יש בגומה ספציפית
            if (tempnum != 0) {
                AddSeed(tempnum, i, whichseed);
                whichseed += tempnum;

            }
            i++;

        }
    }
}


function ValueBoard(boardcheck) { //פונקציית הניקוד
    //תור נוסף מקבל ערך של 2 גולות
    var nikud = 0; 
    var marblecomp = 0;
    var marblep1 = 0;
    var anotherturnval = 0;
    var p1takeseedsnot = 0;
    var potentialloss = 0;
    var last = true;
    var seedlost = 0;
    if (anotherturnboolp2 === true) {
        anotherturnval = anotherturnval + 2;
        anotherturnboolp2 = false;
    }
    if (anotherturnboolp1 === true) {
        anotherturnval = anotherturnval - 2;
        anotherturnboolp1 = false;
    }
    if (boardcheck[1][1] != 1)
        last = false;
    if (whichcolzero.length != 0) {
        for (var s = 0; s < whichcolzero.length; s++) {
            var col = whichcolzero[s];
            if (boardcheck[0][col] != 0) { //אם המחשב הצליח למלא גומה ששחקן 1 היה יכול לגנוב ממנה הוא מקבל ניקוד על הדבר
                p1takeseedsnot = p1takeseedsnot + (boardcheck[0][col] + boardcheck[1][col + 1]);

            }
        }
    }
    for (var i = 0; i < 2; i++) {
        for (var k = 0; k < temppits + 1; k++) {
            if (i === 0 && boardcheck[0][k] === (temppits - k)) { //אם יש אפשרות לתורות נוספים לשחקן 1
                anotherturnval = anotherturnval - 2;
            }
            if (i === 1 && boardcheck[1][k] === k) {//אם יש אפשרות לתורות נוספים למחשב
                anotherturnval = anotherturnval + 2;
            }
            if (k != temppits && i === 0 && boardcheck[0][k] === 0 && boardcheck[1][k + 1] != 0) { 
                // אם המהלך *מסתיים* במצב שבו יש אפשרות לשחקן 1 לגנוב מהמחשב
                var f;
                for (f = 0; f < temppits; f++) { //
                    if (boardcheck[0][f] === (k - f)) { //אם נוצר מצב שכזה ללוח ירד הניקוד
                        potentialloss = potentialloss - (boardcheck[0][k] + boardcheck[1][k + 1]);
                    }
                }

            }
            if (boardcheck[1][1] === 1 && k != 0 && k != 1) {
                if (boardcheck[i][k] != 0)
                    last = false; //בדיקה אם בעקבות המהלך זה התור האחרון
            }
           
        }
    }
    if (anotherturnval < -2) {
        anotherturnval = anotherturnval * 2;
    }
    if (last === true) { //במקרים מסוימים עדיף שלא להגיע לסיום המשחק, ולכן יחושב ההפרש בין הגולות כדי לראות אם כדאי להגיע למהלך האחרון
        for (var k = 0; k < temppits - 1; k++) {
            seedlost = seedlost - boardcheck[0][k];
        }
        nikud = (boardcheck[1][0] - boardcheck[0][temppits]) + anotherturnval + seedlost;
    }
    else
        nikud = (boardcheck[1][0] - boardcheck[0][temppits]) + anotherturnval + p1takeseedsnot + potentialloss;
    return nikud;
}

function CopyArray(originalArray) { //פונקציה להעתקת מערכים
    var newArray = [];
    for (var i = 0; i < originalArray.length; i++)
        newArray[i] = originalArray[i].slice();
    return newArray;
}


function StartGame(currentThis) { //המשחק
    var CurrentId = currentThis.id;
    var CurrentRow = Math.floor(CurrentId / (temppits+1));
    var CurrentCol = CurrentId % (temppits + 1);

    if (turn && mancala[CurrentRow][CurrentCol] != 0 && CurrentRow == 0 && CurrentCol != temppits) { 
        TurnPlayer1(CurrentId); //תור שחקן 1
        turn = !turn; // העברת תור לאחר מכן


    }
    IsEndGame(); // בדיקה אם נגמר המשחק לפי הכללים

}

function TurnPlayer1(CurrentId) { // תור שחקן 1

    document.getElementById("turns").innerHTML = "It's the computer's turn! Click here -> ";
    document.getElementById("CT").style.display = "inline-block";
    var CurrentRow = Math.floor(CurrentId / (temppits+1));
    var CurrentCol = CurrentId % (temppits+1);
    var tempseed = mancala[CurrentRow][CurrentCol]; // משתנה שמכיל את מספר הגולות בגומה שנלחצה
    var numseedend = 0; //משתנה שנועד לעלות באחד כדי לדעת מתי לסיים את ההעברה של האבנים
    mancala[CurrentRow][CurrentCol] = 0; // איפוס הגומה שנלחצה
    while (numseedend != tempseed) {
        while (CurrentCol < temppits) { //התקדמות לעבר הקופה
            CurrentCol++;
            mancala[CurrentRow][CurrentCol] += 1;
            numseedend++;
            if (numseedend === tempseed) {
                if (mancala[CurrentRow][CurrentCol] - 1 == 0 && mancala[CurrentRow + 1][CurrentCol + 1] != 0 && CurrentCol != temppits) {
                    mancala[0][temppits] += mancala[CurrentRow + 1][CurrentCol+1] + 1;
                    mancala[CurrentRow][CurrentCol] = 0;
                    mancala[CurrentRow + 1][CurrentCol+1] = 0;
                   // בדיקה אם הייתה העברה של גולה למקום ריק. אם כן תתבצע אכילה של הגולות של השחקן השני שנמצאות בצד שלו 'באותה' העמודה
                }
                break;
            }
        }

        if (numseedend == tempseed && CurrentCol != temppits) 
            break;
        

        if (CurrentCol === temppits) { // הגעה לקופה
            if (numseedend == tempseed) { // השחקן יקבל תור נוסף אם הגולה האחרונה שלו נפלה בקופה
                turn = !turn;
                alert(p1name + " gets another turn!");
                document.getElementById("turns").innerHTML = "It's your turn, " + p1name;
                document.getElementById("CT").style.display = "none";
                break;
            }
            CurrentRow++;

            while (CurrentCol > 0) {//התקדמות נגד הקופה
                mancala[CurrentRow][CurrentCol] += 1;
                CurrentCol--;
                numseedend++;
                if (numseedend == tempseed)
                    break;
            }
        }
        if (numseedend == tempseed)
            break;

        if (CurrentCol == 0) { // חזרה לשורה הראשונה, שהיא בעצם ה"צד" של השחקן הראשון
            CurrentCol = CurrentCol -  1;
            CurrentRow--;
        }

    }
    
}

function CheckPlayer1(p, ezerboard2) { // תור שחקן 1
    document.getElementById("turns").innerHTML = "It's the computer's turn! Click here -> ";
    document.getElementById("CT").style.display = "inline-block";
    var CurrentRow = 0;
    var CurrentCol = p;
    var tempboard = ezerboard2;
    var tempseed = tempboard[CurrentRow][CurrentCol]; // משתנה שמכיל את מספר הגולות בגומה שנלחצה
    var numseedend = 0; //משתנה שנועד לעלות באחד כדי לדעת מתי לסיים את ההעברה של האבנים
    tempboard[CurrentRow][CurrentCol] = 0; // איפוס הגומה שנלחצה
    while (numseedend != tempseed) {
        while (CurrentCol < temppits) { //התקדמות לעבר הקופה
            CurrentCol++;
            tempboard[CurrentRow][CurrentCol] += 1;
            numseedend++;
            if (numseedend === tempseed) {
                if (tempboard[CurrentRow][CurrentCol] - 1 == 0 && tempboard[CurrentRow + 1][CurrentCol + 1] != 0 && CurrentCol != temppits) {
                    tempboard[0][temppits] += tempboard[CurrentRow + 1][CurrentCol + 1] + 1;
                    tempboard[CurrentRow][CurrentCol] = 0;
                    tempboard[CurrentRow + 1][CurrentCol + 1] = 0;
                    // בדיקה אם הייתה העברה של גולה למקום ריק. אם כן תתבצע אכילה של הגולות של השחקן השני שנמצאות בצד שלו 'באותה' העמודה
                }
                break;
            }
        }

        if (numseedend == tempseed && CurrentCol != temppits)
            break;


        if (CurrentCol === temppits) { // הגעה לקופה
            if (numseedend == tempseed) { // השחקן יקבל תור נוסף אם הגולה האחרונה שלו נפלה בקופה
                turn = !turn;
                anotherturnboolp1 = true;
                break;
            }
            CurrentRow++;

            while (CurrentCol > 0) {//התקדמות נגד הקופה
                tempboard[CurrentRow][CurrentCol] += 1;
                CurrentCol--;
                numseedend++;
                if (numseedend == tempseed)
                    break;
            }
        }
        if (numseedend == tempseed)
            break;

        if (CurrentCol == 0) { // חזרה לשורה הראשונה, שהיא בעצם ה"צד" של השחקן הראשון
            CurrentCol = CurrentCol - 1;
            CurrentRow--;
        }

    }

}


function CheckPlayer2(k,ezerboard) { // שחקן 2
    document.getElementById("turns").innerHTML = "It's your turn, " + p1name;
    document.getElementById("CT").style.display = "none";
    var CurrentRow = 1;
    var CurrentCol = k;
    var tempboard = ezerboard;
    var tempseed = tempboard[CurrentRow][CurrentCol];
    tempboard[CurrentRow][CurrentCol] = 0;
    var numseedend = 0;
    while (numseedend != tempseed) {
        while (CurrentCol > 0) { //התקדמות לעבר הקופה
            CurrentCol--;
            tempboard[CurrentRow][CurrentCol] += 1;
            numseedend++;
            if (numseedend == tempseed) {
                if (tempboard[CurrentRow][CurrentCol] - 1 == 0 && tempboard[CurrentRow - 1][CurrentCol-1] != 0 && CurrentCol != 0) {
                    tempboard[1][0] += tempboard[CurrentRow - 1][CurrentCol-1] + 1;
                    tempboard[CurrentRow][CurrentCol] = 0;
                    tempboard[CurrentRow - 1][CurrentCol-1] = 0;
                    // בדיקה אם הייתה העברה של גולה למקום ריק. אם כן תתבצע אכילה של הגולות של השחקן השני שנמצאות בצד שלו 'באותה' העמודה
                }
                break;
            }
        }

        if (numseedend == tempseed && CurrentCol != 0) 
            break;

        if (CurrentCol == 0) { // הגעה לקופה
            if (numseedend == tempseed) { // אם הגולה האחרונה נפלה בקופה השחקן מקבל תור נוסף
                anotherturnboolp2 = true;
                break;
            }
            CurrentRow--;
            while (CurrentCol < temppits) {//התקדמות נגד הקופה
                tempboard[CurrentRow][CurrentCol] += 1;
                CurrentCol++;
                numseedend++;
                if (numseedend == tempseed)
                    break;
            }
        }

        if (CurrentCol == temppits) { // חזרה לצד שלו
            CurrentCol = CurrentCol + 1;
            CurrentRow++;
        }


    }
}

function IsEndGame() { // בדיקה האם נגמר המשחק
    var nonep1 = true; // האם נגמרו לשחקן הראשון כל הגולות בצד שלו
    var nonep2 = true;// האם נגמרו לשחקן השני כל הגולות בצד שלו
    var i, k;
    for (i = 0; i < 2; i++) {
        for (k = 0; k < (temppits+1); k++) {
            if (i == 0 && k != temppits && mancala[i][k] != 0)
                nonep1 = false;
            if (i == 1 && k != 0 && mancala[i][k] != 0)
                nonep2 = false;
        }
    }
    if (nonep1 || nonep2) {
        if (nonep1) {
            for (i = 1; i < (temppits+1); i++) {
                mancala[1][0] += mancala[1][i];
                mancala[1][i] = 0;
            }
        }
        else if (nonep2) {
            for (i = 0; i < temppits; i++) {
                mancala[0][temppits] += mancala[0][i];
                mancala[0][i] = 0;
            }
        }

        if (mancala[0][temppits] > mancala[1][0]) { // נצחון שחקן 1
            document.getElementById("CT").style.display = "none";
            alert("Player 1 won!");
            document.getElementById("turns").innerHTML = "<div class='winner'> Hurray! " + p1name + " won! </div>";
            AudioWin.play();
            document.getElementById("Start").outerHTML = " ";
            document.getElementById("End").outerHTML += "</br> <div class='containerend'>" + "<div class='endfont'>  <div class='player2name'>" + p2name + "</div>" + " captured " + mancala[1][0] + " seeds </div>" + "</div>";
            document.getElementById("End").outerHTML += "</br> <div class='containerend'>" + "<div class='endfont'>  <div class='player1name'>" + p1name + "</div>" + " captured " + mancala[0][temppits] + " seeds </div>" + "</div>";

            
        }
        else if (mancala[0][temppits] == mancala[1][0]) { // תיקו
            document.getElementById("CT").style.display = "none";
            alert("Tie!");
            document.getElementById("turns").innerHTML = "<div class='winner'> Oh! We have a tie! </div>";
            AudioTie.play();
            document.getElementById("Start").outerHTML = "  ";
            document.getElementById("End").outerHTML += "<h1 style='text-align:center; color:purple'> Both of you captured " + mancala[0][temppits] + " seeds </h1 > ";
        }
        else { // נצחון שחקן 2
            document.getElementById("CT").style.display = "none";
            alert("Player 2 won!");
            document.getElementById("turns").innerHTML = "<div class='winner'> Hurray! " + p2name + " won! </div>";
            AudioWin.play();
            document.getElementById("Start").outerHTML = "  ";
            document.getElementById("End").outerHTML += "</br> <div class='containerend'>" + "<div class='endfont'>  <div class='player1name'>" + p1name + "</div>" + " captured " + mancala[0][temppits] + " seeds </div>" + "</div>";
            document.getElementById("End").outerHTML += "</br> <div class='containerend'>" + "<div class='endfont'>  <div class='player2name'>" + p2name + "</div>" + " captured " + mancala[1][0] + " seeds </div>" + "</div>";
       
        }
            


    }

}


// בשביל כפתור הוראות 
var modal = document.querySelector(".modal");
var trigger = document.querySelector(".trigger");
var closeButton = document.querySelector(".close-button");

function toggleModal() {
    modal.classList.toggle("show-modal");
}

function windowOnClick(event) {
    if (event.target === modal) {
        toggleModal();
    }
}

trigger.addEventListener("click", toggleModal);
closeButton.addEventListener("click", toggleModal);
window.addEventListener("click", windowOnClick);